//Name: Mohamed Aljak
//Student ID: 2237281

package application;

import vehicles.Bicycle;

public class BikeStore{
    public static void main(String[] args){
        Bicycle[] bicycles = new Bicycle[]{
            new Bicycle("Trek", 5, 26),
            new Bicycle("Cannondale", 12, 29),
            new Bicycle("GT", 26, 23),
            new Bicycle("DiamondBack", 1, 23)
        };

        for(Bicycle bicycle: bicycles){
            System.out.println(bicycle);
        }
    }
}
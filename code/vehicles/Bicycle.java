//Name: Mohamed Aljak
//Student ID: 2237281

package vehicles;
public class Bicycle{
    String manufacturer;
    int numberGears;
    double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    public int numberGears(){
        return this.numberGears;
    }
    public double maxSpeed(){
        return this.maxSpeed;
    }

    @Override
    public String toString(){
        StringBuilder bicycleInfo =  new StringBuilder();
        bicycleInfo.append("Manufacturer: ").append(this.manufacturer);
        bicycleInfo.append(", Number of Gears: ").append(this.numberGears);
        bicycleInfo.append(", MaxSpeed: ").append(this.maxSpeed);
        return bicycleInfo.toString();
    }
}